# WebInterpret Customer API

Customer API is used to manage customers. 

## Exposed endpoints:

#### Create a new customer  
[POST] /customers  
{
    "customer_id": 0,
    "username": ""
}

#### Get customer by username
[GET] /customers/<user_name>

#### Get customer by customer_id
[GET] /customers/<customer_id>

## Package installation
`webinterpret` sample project is a package so you can simply install it using pip:
```shell script
pip install . --upgrade
``` 

After successful installation you should have a package named `wi_customer` in your site-packages folder. 

To confirm that just use `pip show wi_customer`:
```shell script
$ pip show wi_customer
Name: wi-customer
Version: 0.1
Summary: WebInterpret Customer API
Home-page: https://gitlab.com/alireza.stack/wi-customer
Author: Alireza Hoseini
Author-email: alireza.stack@gmail.com
License: UNKNOWN
Location: /usr/local/lib/python3.8/site-packages
Requires: pymongo, flask, Flask-RESTful, pytest
Required-by:
``` 

There is a uWSGI ini file in `wi_customer` package that you can run using `uWSGI`:

```shell script
uwsgi --ini uwsgi.ini
```

## Run tests
In order to simply run tests, head over to tests directory and issue the `pytest` command.
It should run 5 test cases with the below output:  
`====================== 5 passed in 0.29s ======================`