"""Setup tools packaging information."""

from setuptools import setup, find_packages
import os

here = os.path.abspath(os.path.dirname(__file__))

requires = [
    "flask",
    "pytest",
    "Flask-RESTful",
    "pymongo",
]

open(os.path.join(here, "requirements.txt"), "w").writelines(
    [line + "\n" for line in requires]
)

setup(
    name="wi-customer",
    version="0.1",
    description="WebInterpret Customer API",
    long_description="WebInterpret Customer API is used to interact with customer resources.",
    classifiers=[
        "Programming Language :: Python",
        "Framework :: Flask",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ],
    author="Alireza Hoseini",
    author_email="alireza.stack@gmail.com",
    url="https://gitlab.com/alireza.stack/wi-customer",
    keywords="Threatify",
    packages=find_packages(),
    package_data={'': ['uwsgi.ini']},
    include_package_data=True,
    zip_safe=False,
    test_suite="wi_customer",
    install_requires=requires,
)
