

class TestCustomer(object):
    def test_invalid_customer_id(self, client):
        response = client.get(f'/customers/1000')
        result = response.get_json()

        assert response.status_code == 404
        assert type(result) == dict
        assert 'error' in result
        assert result['error']['message'] == 'customer not found'

    def test_invalid_customer_name(self, client):
        response = client.get(f'/customers/myUsername')
        result = response.get_json()

        assert response.status_code == 404
        assert type(result) == dict
        assert 'error' in result
        assert result['error']['message'] == 'customer not found'

    def test_get_valid_customer_by_id(self, client, fresh_database):
        response = client.get(f'/customers/100')
        result = response.get_json()

        assert response.status_code == 200
        assert 'data' in result
        assert result['data']['customer_id'] == 100

    def test_get_valid_customer_by_username(self, client, fresh_database):
        response = client.get(f'/customers/john')
        result = response.get_json()

        assert response.status_code == 200
        assert 'data' in result
        assert result['data']['customer_id'] == 100

    def test_create_customer_successfully(self, client, fresh_database):
        username = 'new_customer'
        response = client.post(f'/customers', json={'customer_id': 206, 'username': username})
        customer_doc = fresh_database.customers.find_one({'customer_id': 206})

        assert response.status_code == 204
        assert customer_doc is not None
        assert customer_doc['username'] == username
