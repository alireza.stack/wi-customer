"""Test cases configuration."""

from wi_customer.mongodb_store import MongoDBStore
from wi_customer import create_app
import pytest


@pytest.yield_fixture(scope="session")
def app():
    """
    Flask test app setup code, this only gets executed once.
    :return: Flask app
    """

    _app = create_app()
    _app.config["TESTING"] = True

    # Establish an application context before running the tests.
    ctx = _app.app_context()
    ctx.push()

    yield _app

    ctx.pop()

    # code below called after fixture goes out of scope
    return None


@pytest.yield_fixture(scope="function")
def client(app):
    """
    Client app setup code, this gets executed for each test function.

    :param app: Pytest fixture
    :return: Flask app client
    """
    yield app.test_client()


@pytest.yield_fixture(scope="function")
def fresh_database():
    """
    fresh database will wipe old data out and insert new customer data
    :return: MongoDB Collection instance
    """
    mongo_store = MongoDBStore()
    customer_collection = mongo_store.database.customers
    customer_collection.remove({})
    customer_collection.insert_one({
        'customer_id': 100,
        'username': 'john'
    })
    yield mongo_store.database
