"""Package Initialization."""

from .routes import app_routes
from flask import Flask


def create_app(settings_override=None, load_routes=True, test_mode=False):
    """Application factory."""
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    if settings_override:
        app.config.update(settings_override)

    if load_routes:
        app_routes(app)

    return app
