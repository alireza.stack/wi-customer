"""Customer API endpoint."""

from ..repos.customer_repo import CustomerRepository
from ..mongodb_store import MongoDBStore
from ..models.customer import Customer
from flask_restful import Resource
from flask import request


class CustomerBase(object):
    def __init__(self):
        self.store = MongoDBStore()
        self.repo = CustomerRepository(self.store.database.customers)


class CustomerByIdView(Resource, CustomerBase):
    def __init__(self):
        super().__init__()

    def get(self, customer_id):
        customer = self.repo.get(customer_id)
        if not customer:
            return {
                       "error": {
                           "message": "customer not found"
                       }
                   }, 404

        return {
                   "data": customer.to_json()
               }, 200


class CustomerByNameView(Resource, CustomerBase):
    def __init__(self):
        super().__init__()

    def get(self, username):
        customer = self.repo.find(username)
        if not customer:
            return {
                       "error": {
                           "message": "customer not found"
                       }
                   }, 404

        return {
                   "data": customer.to_json()
               }, 200


class CustomersView(Resource, CustomerBase):
    def __init__(self):
        super().__init__()

    def post(self):
        data = request.get_json()
        # for larger json payloads we can use jsonschema alternatively
        if 'customer_id' not in data or 'username' not in data:
            return {
                       "error": {
                           "message": "username and/or customer_id is missing!"
                       }
                   }, 400

        customer = Customer(
            customer_id=int(data["customer_id"]),
            username=data["username"],
        )
        self.repo.set(customer)
        return {}, 204
