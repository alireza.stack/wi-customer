"""Application routes."""

from .views.customer_info import CustomersView, \
    CustomerByIdView, CustomerByNameView
from flask_restful import Api


def app_routes(app):
    api = Api(app)

    """Application routes configuration."""
    api.add_resource(CustomersView, '/customers')
    api.add_resource(CustomerByNameView, '/customers/<string:username>')
    api.add_resource(CustomerByIdView, '/customers/<int:customer_id>')
