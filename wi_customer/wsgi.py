from wi_customer import create_app as application

if __name__ == "__main__":
    # if script is run directly start server
    application = application()
    application.run()
else:
    # uWSGI executes run() itself so just initiate app
    app = application()
