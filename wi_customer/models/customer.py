from dataclasses import dataclass


@dataclass
class Customer(object):
    customer_id: int
    username: str

    def __init__(self, customer_id, username):
        self.customer_id = customer_id
        self.username = username

    def to_json(self) -> dict:
        return {
            'customer_id': self.customer_id,
            'username': self.username
        }
