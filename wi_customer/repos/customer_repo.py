
from pymongo.collection import Collection
from ..models.customer import Customer


class CustomerRepository(object):
    def __init__(self, collection: Collection):
        self.collection = collection

    def set(self, customer: Customer):
        self.collection.insert_one(customer.to_json())

    def get(self, customer_id):
        customer_doc = self.collection.find_one({
            'customer_id': customer_id
        })
        if not customer_doc:
            return None

        return self._to_customer(customer_doc)

    def find(self, username) -> Customer:
        customer_doc = self.collection.find_one({
            'username': username
        })
        if not customer_doc:
            return None

        return self._to_customer(customer_doc)

    def _to_customer(self, customer_doc):
        customer = Customer(
            customer_id=customer_doc['customer_id'],
            username=customer_doc['username']
        )
        return customer
