
from pymongo import MongoClient
import os


class MongoDBStore(object):
    def __init__(self, host='localhost', port=27017, database='webinterpret'):
        # OS env has higher precedence
        host = os.environ.get('MONGODB_URI', host)
        _client = MongoClient(host=host, port=port)
        self.database = _client[database]
